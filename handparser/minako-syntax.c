#include <stdio.h>
#include <stdlib.h>
#include "minako.h"

void eat(void);
int isToken(int);
void isTokenAndEat(int);

void nt_program(void);
void nt_functiondefinition(void);
void nt_functioncall(void);
void nt_statementlist(void);
void nt_block(void);
void nt_statement(void);
void nt_ifstatement(void);
void nt_returnstatement(void);
void nt_printf(void);
void nt_type(void);
void nt_statassignment(void);
void nt_assignment(void);
void nt_expr(void);
void nt_simpexpr(void);
void nt_term(void);
void nt_factor(void);

/* ---------------------------
global variables
------------------------------
*/

int currentToken;
int nextToken;

int main(int argc, char **argv) {

    if (argc != 2) {
        yyin = stdin;
    } else {
        yyin = fopen(argv[1], "r");
        if (yyin == 0) {
            fprintf(
                    stderr,
                    "Fehler: Konnte Datei %s nicht zum lesen oeffnen. (eventuell wurden zu viele parameter angegeben. Erwartete Parameteranzahl: 1\n",
                    argv[1]
            );
            exit(-1);
        }
    }

    currentToken = yylex();
    nextToken = yylex();

    nt_program();

    return 0;

}

void eat(void) {
    currentToken = nextToken;
    nextToken = yylex();
}

int isToken(int tok) {
    return (currentToken == tok);
}

void isTokenAndEat(int tok) {

    if (isToken(tok)) {
        eat();
    }
    else {
        fprintf(stderr, "ERROR: syntax error in line %d. ", yylineno);
        fprintf(stderr, "Cannot eat token ");
        if (currentToken <= 255)
            printf("'%c'", currentToken);
        else
            printf("%d", currentToken);
        
        printf(". \n");

        exit(-1);
    }

}

// nt = nicht terminal

void nt_program(void) {

    while (isToken(KW_BOOLEAN) || isToken(KW_FLOAT) || isToken(KW_INT) || isToken(KW_VOID)) {
        nt_functiondefinition();
    }
    isTokenAndEat(EOF);

}

void nt_functiondefinition(void) {
    nt_type(); isTokenAndEat(ID); isTokenAndEat('('); isTokenAndEat(')');
    isTokenAndEat('{'); nt_statementlist(); isTokenAndEat('}');
}

void nt_functioncall(void) {
    isTokenAndEat(ID); isTokenAndEat('('); isTokenAndEat(')');
}

void nt_statementlist(void) {
    while (isToken('{') || isToken(KW_IF) || isToken(KW_RETURN) || isToken(KW_PRINTF) || isToken(ID) ) {
        nt_block();
    }
}

void nt_block(void) {
    if (isToken('{')) {
        eat();
        nt_statementlist();
        isTokenAndEat('}');
    } else {
        nt_statement();
    }
}

void nt_statement(void) {
    switch (currentToken) {
        case KW_IF:
            nt_ifstatement(); break;
        case KW_RETURN:
            nt_returnstatement(); isTokenAndEat(';'); break;
        case KW_PRINTF:
            nt_printf(); isTokenAndEat(';'); break;
        case ID:
            if (nextToken == '=') {
                nt_statassignment(); isTokenAndEat(';');
            }
            else if (nextToken == '(') {
                nt_functioncall(); isTokenAndEat(';');
            }
            else {
                printf("ERROR: cannot find '=' or '(' after ID\n");
            }
            break;
        default:
            printf("ERROR: no beginning of statement\n");
    }
}

void nt_ifstatement(void) {
    isTokenAndEat(KW_IF);
    isTokenAndEat('('); nt_assignment(); isTokenAndEat(')');
    nt_block();
}

void nt_returnstatement(void) {
    isTokenAndEat(KW_RETURN);
    if (isToken(ID) || isToken('-') || isToken(CONST_INT) || isToken(CONST_FLOAT) || isToken(CONST_BOOLEAN)
        || isToken('(')
    )
        nt_assignment();
    else {
        // do nothing because '?' in the ebnf means optional
    }   
}

void nt_printf(void) {
    isTokenAndEat(KW_PRINTF);
    isTokenAndEat('('); nt_assignment(); isTokenAndEat(')');
}

void nt_type(void) {
    switch(currentToken) {
        case KW_BOOLEAN:
        case KW_FLOAT:
        case KW_INT:
        case KW_VOID:
            eat(); break;
        default:
            fprintf(stderr, "ERROR: during parsing (function %s)", __func__);
            fprintf(stderr, " in line %d.\n", yylineno);
    }
}

void nt_statassignment(void) {
    isTokenAndEat(ID);
    isTokenAndEat('=');
    nt_assignment();
}

void nt_assignment(void) {
    if (isToken(ID) && nextToken == '=') {
        nt_statassignment();
    } else if (isToken (ID) || isToken('-') || isToken(CONST_INT) || isToken(CONST_FLOAT) || isToken(CONST_BOOLEAN)
            || isToken('(')
        ) {
            nt_expr();
    } else {
        printf("ERROR: expected assignment or expression\n");
    }
}

void nt_expr(void) {
    nt_simpexpr();

    if (isToken(EQ) || isToken(NEQ) || isToken(LEQ) 
        || isToken(GEQ)  || isToken(LSS) || isToken(GRT)) {
        eat();
        nt_simpexpr();
    } else {
        // do nothing ('?' means optional)
    }
}

void nt_simpexpr(void) {
    if (isToken('-'))
        eat();
    nt_term();
    while (isToken('+') || isToken('-') || isToken(OR)) {
        eat();
        nt_term();
    }
}

void nt_term(void) {
    nt_factor();
    while (isToken('*') || isToken('/') || isToken(AND)) {
        eat();
        nt_factor();
    }
}

void nt_factor(void) {
    switch (currentToken) {
        case CONST_INT:
        case CONST_FLOAT:
        case CONST_BOOLEAN:
            eat(); break;
        case ID:
            if (nextToken == '(')
                nt_functioncall();
            else
                eat();
            break;
        case '(':
            eat(); nt_assignment(); isTokenAndEat(')');
    }
}
